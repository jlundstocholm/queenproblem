﻿using System;

namespace QueenProblem
{
    public class ChessPiece
    {
        private Coordinate _location;

        public ChessPiece(int horizontal, int vertical)
        {
            _location.Horizontal = horizontal;
            _location.Vertical = vertical;
        }

        public Coordinate Location 
        { 
            get 
            {
                return _location;
            }

            set 
            {
                _location = value;
            }
        }

        public override string ToString()
        {
            return string.Format("[{0},{1}]", Location.Horizontal, Location.Vertical);
        }

        public Coordinate GetLeftBottomBound()
        {
            var minHorizontal = Math.Min(Location.Horizontal, Location.Vertical);
            return new Coordinate {Horizontal = Location.Horizontal - minHorizontal, Vertical = Location.Vertical - minHorizontal};
        }

        public Coordinate GetLeftTopBound(int dimension)
        {
            var upperBound = new Coordinate {Horizontal = Location.Horizontal, Vertical = Location.Vertical};

            while (upperBound.Horizontal > 0 && upperBound.Vertical < dimension - 1)
            {
                upperBound.Horizontal--;
                upperBound.Vertical++;
            }
            return upperBound;
        }
    }

}
