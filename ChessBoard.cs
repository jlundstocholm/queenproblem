﻿using System;
using System.Collections.Generic;
using System.Linq;
using QueenProblem.Extensions;

namespace QueenProblem
{
    public class ChessBoard
    {

        public int NumberOfAvailableSquares
        {
            get
            {
                int squareCount = 0;
                foreach (var square in Squares)
                {
                    if (!square.IsRemoved)
                        squareCount++;
                }
                return squareCount;
            }
        }

        public readonly Square[,] Squares;

        public List<Coordinate> GetListOfOccupiedSquares()
        {
            var list = new List<Coordinate>();

            foreach (var square in Squares)
            {
                if (square.HasPiece)
                {
                    list.Add(square.Coordinate);
                }
            }

            return list;
        }

        public ChessBoard()
        {
            const int dimension = 8;
            Squares = new Square[dimension, dimension];
            PopulateBoard(dimension);
            Coordinates = new List<Coordinate>();
        }

        public List<Coordinate> Coordinates { get; set; }

        public ChessBoard(int dimension)
        {
            Squares = new Square[dimension,dimension];
            PopulateBoard(dimension);
            Coordinates = new List<Coordinate>();
        }

        private void PopulateBoard(int dimension)
        {
            for (var i = 0; i < dimension; i++)
            {
                for (var j = 0; j < dimension; j++)
                    Squares[i, j] = new Square
                    {
                        Coordinate = new Coordinate {Horizontal = i, Vertical = j},
                    };
            }
        }

        public int GetRemainingAvailableSquares(ChessPiece piece, Square[,] squares)
        {
            int count = 0;

            // Check horizontal
            for (var i = 0; i < squares.GetLength(0); i++)
            {
                var square = squares[i, piece.Location.Vertical];
                if (!square.HasBeenChecked && !square.IsRemoved)
                {
                    count++;
                    square.HasBeenChecked = true;
                }
            }

            // Check vertical
            for (var i = 0; i < Squares.GetLength(0); i++)
            {
                var square = squares[piece.Location.Horizontal, i];
                if (!square.HasBeenChecked & !square.IsRemoved)
                {
                    count++;
                    square.HasBeenChecked = true;
                }
            }

            // Check diagonal up, from left
            var leftBottomBound = piece.GetLeftBottomBound();

            while (Math.Max(leftBottomBound.Horizontal, leftBottomBound.Vertical) < Squares.GetLength(0))
            {
                var square = squares[leftBottomBound.Horizontal, leftBottomBound.Vertical];
                if (!square.HasBeenChecked && !square.IsRemoved)
                {
                    count++;
                    square.HasBeenChecked = true;
                }

                leftBottomBound.Horizontal++;
                leftBottomBound.Vertical++;
            }

            // Check diagonal down, from left

            var leftUpperBound = piece.GetLeftTopBound(Squares.GetLength(0));

            while (leftUpperBound.Horizontal < Squares.GetLength(0) && leftUpperBound.Vertical >= 0)
            {
                var square = squares[leftUpperBound.Horizontal, leftUpperBound.Vertical];
                if (!square.HasBeenChecked && !square.IsRemoved)
                {
                    count++;
                    square.HasBeenChecked = true;
                }

                leftUpperBound.Horizontal++;
                leftUpperBound.Vertical--;
            }

            squares.ClearMarks();

            return NumberOfAvailableSquares - count;
        }
        
        public bool Put()
        {
            // Get number of remaining squares for each possible available location on the board.
            foreach (var square in Squares)
            {
                var piece = new ChessPiece(square.Coordinate.Horizontal, square.Coordinate.Vertical);

                if (!square.IsRemoved)
                    square.AvailableSquares = GetRemainingAvailableSquares(piece, Squares);
            }

            var listOfSortedSquares = Squares.Sort();
            //Pick first square in sorted list that has not been removed
            var pickedSquare = listOfSortedSquares.FirstOrDefault(sq => !sq.IsRemoved);
            

            if (pickedSquare != null)
            {
                this.PutPieceDown(new ChessPiece(pickedSquare.Coordinate.Horizontal, pickedSquare.Coordinate.Vertical));
                Coordinates.Add(new Coordinate { Horizontal = pickedSquare.Coordinate.Horizontal, Vertical = pickedSquare.Coordinate.Vertical });
                return true;
            }
            return false;
        }

        void PutPieceDown(ChessPiece piece)
        {
            Squares.SetValue(new Square { HasPiece = true, IsRemoved = true, Coordinate = new Coordinate { Horizontal = piece.Location.Horizontal, Vertical = piece.Location.Vertical } }, piece.Location.Horizontal, piece.Location.Vertical);
            UpdateBoard(piece);
        }

        private void UpdateBoard(ChessPiece piece)
        {
            // Check horizontal
            for (var i = 0; i < Squares.GetLength(0); i++)
            {
                var square = Squares[i, piece.Location.Vertical];
                square.IsRemoved = true;
            }

            // Check vertical
            for (var i = 0; i < Squares.GetLength(0); i++)
            {
                var square = Squares[piece.Location.Horizontal, i];
                square.IsRemoved = true;
            }

            // Check diagonal up, from left
            var leftBottomBound = piece.GetLeftBottomBound();

            while (Math.Max(leftBottomBound.Horizontal, leftBottomBound.Vertical) < Squares.GetLength(0))
            {
                var square = Squares[leftBottomBound.Horizontal, leftBottomBound.Vertical];
                square.IsRemoved = true;

                leftBottomBound.Horizontal++;
                leftBottomBound.Vertical++;
            }

            // Check diagonal down, from left

            var leftUpperBound = piece.GetLeftTopBound(Squares.GetLength(0));

            while (leftUpperBound.Horizontal < Squares.GetLength(0) && leftUpperBound.Vertical >= 0)
            {
                var square = Squares[leftUpperBound.Horizontal, leftUpperBound.Vertical];
                square.IsRemoved = true;

                leftUpperBound.Horizontal++;
                leftUpperBound.Vertical--;
            }
        }
    }
}
