﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QueenProblem.Extensions
{
    public static class SquareExtensions
    {
        public static string[] Join(this List<Coordinate> squares)
        {
            return squares.Select(i => string.Format("[{0},{1}]", i.Horizontal, i.Vertical)).ToArray();
        }

        public static void ClearMarks(this Square[,] squares)
        {
            foreach (var square in squares)
            {
                square.HasBeenChecked = false;
            }
        }

        public static List<Square> Sort(this Square[,] squares)
        {
            var listOfSquares = new List<Square>();

            foreach (var square in squares)
            {
                listOfSquares.Add(square);
            }
            listOfSquares.Sort();
            return listOfSquares;
        }
    }
}
