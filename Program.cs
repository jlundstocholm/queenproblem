﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QueenProblem
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("This program will put as many queens as possible on a chess board\n" +
                              "- with neither being able to strike another queen.\n" +
                          "\n" +
                          "This is the so-called 'Queen problem'\n" +
                          "\n"
                          );
            Console.WriteLine("Press any key to start ...");
            Console.Read();

            var board = new ChessBoard();

            while (board.Put()) {}

            Console.Write("Queens put down:");
            board.Coordinates.ForEach(i => Console.Write(i));
            Console.WriteLine("\n");

            Console.Write(string.Format("A total of {0} queens were put down on the board.\n", board.Coordinates.Count));

            Console.ReadKey();
        }
    }
}
