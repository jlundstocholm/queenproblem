﻿using System;

namespace QueenProblem
{
    public class Square : IComparable<Square>, IComparable
    {
        public Coordinate Coordinate { get; set; }
        public bool HasPiece { get; set; }
        public bool HasBeenChecked { get; set; }
        public int AvailableSquares { get; set; }
        public bool IsRemoved { get; set; }

        public int CompareTo(Square other)
        {
            return other.AvailableSquares.CompareTo(this.AvailableSquares);
        }

        public int CompareTo(object obj)
        {
            return (((Square) obj).AvailableSquares).CompareTo(this.AvailableSquares);
        }

        public override string ToString()
        {
            return string.Format("[{0},{1}], Available: {2}, HasPiece: {3}, IsRemoved: {4}", Coordinate.Horizontal, Coordinate.Vertical, AvailableSquares, HasPiece, IsRemoved);
        }
    }
}