﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using QueenProblem;
using QueenProblem.Extensions;

namespace Test.QueenProblem
{
    [TestFixture]
    public class TestChessboard
    {
        [Test]
        [TestCase(8)]
        [TestCase(4)]
        [TestCase(5)]
        public void BoardSquares(int dimension)
        {
            var board = new ChessBoard(dimension);
            var numberOfSquares = board.Squares.Length;
            Assert.AreEqual(dimension*dimension, numberOfSquares);
        }

        [Test]
        [TestCase(0,0,64-22)]
        [TestCase(1,1,64-24)]
        [TestCase(2,2,64-26)]
        [TestCase(3,3,64-28)]
        [TestCase(4,4,64-28)]
        [TestCase(3,1,64-24)]
        [TestCase(5,2,64-26)]
        public void TestRemainingSquaresOnEmptyBoardWith(int h, int v, int numberRemaining)
        {
            var board = new ChessBoard();
            int remainingSquares = board.GetRemainingAvailableSquares(new ChessPiece(h, v), board.Squares);

            Assert.AreEqual(numberRemaining, remainingSquares);
        }

        [Test]
        public void TestSortingOf2DimArray()
        {
            // Arrange
            var board = new ChessBoard(2);
            board.Squares[0, 0] = new Square { AvailableSquares = 2 };
            board.Squares[0, 1] = new Square { AvailableSquares = 1 };
            board.Squares[1, 0] = new Square { AvailableSquares = 4 };
            board.Squares[1, 1] = new Square { AvailableSquares = 3 };

            // Act
            var sortedSquares = board.Squares.Sort();

            // Assert
            Assert.AreEqual(4,sortedSquares[0].AvailableSquares);
            Assert.AreEqual(3,sortedSquares[1].AvailableSquares);
            Assert.AreEqual(2,sortedSquares[2].AvailableSquares);
            Assert.AreEqual(1,sortedSquares[3].AvailableSquares);
        }

        [Test]
        public void Puts()
        {
            var board = new ChessBoard(6);

            // Put first piece
            board.Put();
            Assert.AreEqual(20, board.NumberOfAvailableSquares);
            Assert.AreEqual(1, board.GetListOfOccupiedSquares().Count);

            // Put second piece
            board.Put();
            Assert.AreEqual(10, board.NumberOfAvailableSquares);
            Assert.AreEqual(2, board.GetListOfOccupiedSquares().Count);

            board.Put();
            Assert.AreEqual(5, board.NumberOfAvailableSquares);
            Assert.AreEqual(3, board.GetListOfOccupiedSquares().Count);

            board.Put();
            Assert.AreEqual(2, board.NumberOfAvailableSquares);
            Assert.AreEqual(4, board.GetListOfOccupiedSquares().Count);

            board.Put();
            Assert.AreEqual(0, board.NumberOfAvailableSquares);
            Assert.AreEqual(5, board.GetListOfOccupiedSquares().Count);
        }

        [Test]
        public void Test8x8Board()
        {
            var board = new ChessBoard();

            // Put first piece
            board.Put();
            Assert.AreEqual(42, board.NumberOfAvailableSquares);
            Assert.AreEqual(1, board.GetListOfOccupiedSquares().Count);

            // Put second piece
            board.Put();
            Assert.AreEqual(26, board.NumberOfAvailableSquares);
            Assert.AreEqual(2, board.GetListOfOccupiedSquares().Count);

            board.Put();
            Assert.AreEqual(17, board.NumberOfAvailableSquares);
            Assert.AreEqual(3, board.GetListOfOccupiedSquares().Count);

            board.Put();
            Assert.AreEqual(10, board.NumberOfAvailableSquares);
            Assert.AreEqual(4, board.GetListOfOccupiedSquares().Count);

            board.Put();
            Assert.AreEqual(5, board.NumberOfAvailableSquares);
            Assert.AreEqual(5, board.GetListOfOccupiedSquares().Count);

            board.Put();
            Assert.AreEqual(3, board.NumberOfAvailableSquares);
            Assert.AreEqual(6, board.GetListOfOccupiedSquares().Count);

            board.Put();
            Assert.AreEqual(0, board.NumberOfAvailableSquares);
            Assert.AreEqual(7, board.GetListOfOccupiedSquares().Count);
                
            
        }

        [Test]
        public void CanCreatePieceWithLocation()
        {
            var piece = new ChessPiece(1,1);
            Assert.AreEqual(1, piece.Location.Horizontal);
            Assert.AreEqual(1, piece.Location.Vertical);
        }

        [Test]
        [TestCase(3,9)]
        [TestCase(4,16)]
        public void EmptyBoardWith3X3Has9AvailableSquares(int dimension, int result)
        {
            var board = new ChessBoard(dimension);

            var nonAvailableCoordinates = board.GetListOfOccupiedSquares();

            Assert.AreEqual(result, result - nonAvailableCoordinates.Count);
        }

        [Test]
        public void CanConstructPieceWithLocation()
        {
            var piece = new ChessPiece(1, 1);
            Assert.AreEqual(1, piece.Location.Horizontal);
            Assert.AreEqual(1, piece.Location.Vertical);
        }

   

        [Test]
        public void InitialBoardIsEmpty()
        {
            var board = new ChessBoard(4);
            foreach (var square in board.Squares)
            {
                Assert.AreEqual(false, square.IsRemoved);
            }
        }
    }
}
