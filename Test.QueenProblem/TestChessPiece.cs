﻿using NUnit.Framework;
using QueenProblem;

namespace Test.QueenProblem
{
    [TestFixture]
    public class TestChessPiece
    {
        [Test]
        [TestCase(2,3,0,1)]
        [TestCase(4,3,1,0)]
        [TestCase(4,2,2,0)]
        [TestCase(1,4,0,3)]
        [TestCase(0,0,0,0)]
        [TestCase(5,5,0,0)]
        [TestCase(0,5,0,5)]
        [TestCase(5,0,5,0)]
        public void TestGetLeftLowerBoundLocation(int h, int v, int lh, int lv)
        {
            var piece = new ChessPiece(h, v);

            var leftBound = piece.GetLeftBottomBound();

            Assert.AreEqual(lh, leftBound.Horizontal);
            Assert.AreEqual(lv, leftBound.Vertical);
        }

        [Test]
        [TestCase(2, 3, 0, 5)]
        [TestCase(4, 3, 2, 5)]
        [TestCase(4, 2, 1, 5)]
        [TestCase(1, 4, 0, 5)]
        [TestCase(0, 0, 0, 0)]
        [TestCase(5, 5, 5, 5)]
        [TestCase(0, 5, 0, 5)]
        [TestCase(5, 0, 0, 5)]
        public void TestGetLeftUpperBoundLocation(int h, int v, int lh, int lv)
        {
            var piece = new ChessPiece(h, v);

            var leftBound = piece.GetLeftTopBound(6);

            Assert.AreEqual(lh, leftBound.Horizontal);
            Assert.AreEqual(lv, leftBound.Vertical);
        }

        //[Test]
        //[TestCase(2, 3, 0, 1)]
        //[TestCase(4, 3, 1, 0)]
        //[TestCase(4, 2, 2, 0)]
        //[TestCase(1, 4, 0, 3)]
        //[TestCase(0, 0, 0, 0)]
        //[TestCase(5, 5, 0, 0)]
        //[TestCase(0, 5, 0, 5)]
        //[TestCase(5, 0, 5, 0)]
        //public void TestGetRightBoundLocation(int h, int v, int rh, int rv)
        //{
        //    var piece = new ChessPiece(h, v);
        //    var rightBound = piece.GetRightBound();
        //    Assert.AreEqual(rh, rightBound.Horizontal);
        //    Assert.AreEqual(rv, rightBound.Vertical);

        //}
    }
}