﻿namespace QueenProblem
{
    public struct Coordinate
    {
        public int Horizontal { get; set; }
        public int Vertical { get; set; }

        public override string ToString()
        {
            return string.Format("[{0},{1}]", (Horizontal)Horizontal, Vertical + 1);
        }
    }

    public enum Horizontal
    {
        A = 0,
        B = 1,
        C = 2,
        D = 3,
        E = 4,
        F = 5,
        G = 6,
        H = 7
    }
}